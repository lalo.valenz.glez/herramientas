var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var app = express();

var connection = mysql.createConnection({
	host     : 'localhost',
	user     : 'root',
	password : '',
	database : 'herramientas'
});

connection.connect(function(err){
	if(!err){
		console.log("base de datos conectada...");
		return true;
	}
	else{
		console.log("error al conectar la base de datos");
		return false;
	}
});	

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({ secret: 'herramientas', cookie: { maxAge: 60000 }, resave: true, saveUninitialized: true }));

var session;

app.get('/', function(req, res){
	res.json({status:false, data: [{message: 'yeah'}], error: false});
});

app.get('/tools', function(req, res){
	console.log('GET /tools');
	connection.query('select * from tools',[], function(err, rows){
		if(!err){
			res.json({status: true, data: rows, error: false});
			console.log(rows[0]);
		}else{
			res.json({status: false, data: [], error: true});
			console.log(err);
		}
	});
});

app.post('/tools', function(req, res){
	var name = req.body.name;
	console.log('POST /tools');
	connection.query('insert into tools (name) values(?)',[name], function(err, rows){
		if(!err){
			res.json({status: true, data: rows, error: false});
			console.log(rows[0]);
		}else{
			res.json({status: false, data: [], error: true});
			console.log(err);
		}
	});
});

app.get('/tools/:id', function(req, res){
	console.log('GET /tools/'+ req.params.id);
	var id = req.params.id;
	connection.query('select * from tools where id_tools = ?',[id], function(err, rows){
		if(!err){
			res.json({status: true, data: [{message: 'has seleccionado la herramienta ' + rows[0].id_tools}], error: false});
			console.log(rows);
		}else{
			res.json({status: false, data: [], error: true});
			console.log(err);
		}
	});
});

app.listen(3000, function(){
	console.log('Aplicacion corriendo http://localhost/ en el puerto 3000');
});
